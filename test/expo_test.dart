import 'dart:math';

import 'package:essential_algorithms/essential_algorithms.dart';
import 'package:test/test.dart';

void main() {
  group('Exponentiate test', () {
    test('Exponentiate test', () {
      expect(exponentiate(2,4), equals(16));
      expect(exponentiate(3,3), equals(27));
      expect(exponentiate(2,0), equals(1));
      expect(exponentiate(2,17), pow(2,17));
    });
  });

  group('Find factors', () {
    test('Finding factors', (){
      expect(findFactors(4), equals([2,2]));
    });
  });
}
