import 'dart:math';
import 'dart:core';

import 'package:essential_algorithms/essential_algorithms.dart';
import 'package:test/test.dart';

void main() {
  group('Area test 1', () {
    test('Finding f1 areas', (){
      var area=areaUnderCurveWithRectangleRule(f1, 0,1,2000);
      expect((area-0.3333).abs()/0.3333<0.001, equals(true));
    });
    test('Finding f2 areas', (){
      var area=areaUnderCurveWithRectangleRule(f2, 0,1,2000);
      expect((area-1).abs()/1<0.001, equals(true));
    });
    test('Finding f3 areas', (){
      var area=areaUnderCurveWithRectangleRule(f3, 0, pi, 100);
      expect((area-pi/2).abs()/1<0.001, equals(true));
    });
  });

  group('Area test 2', () {
    test('Finding f1 areas', (){
      var area=areaUnderCurveWithTrapezoidRule(f1, 0,1,50);
      expect(area, closeToPercentage(0.33333, 0.001));
    });
    test('Finding f2 areas', (){
      var area=areaUnderCurveWithTrapezoidRule(f2, 0,1,30);
      expect(area, closeToPercentage(1, 1E-3));
    });
    test('Finding f3 areas', (){
      var area=areaUnderCurveWithRectangleRule(f3, 0, pi, 10);
      expect(area, closeToPercentage(pi/2, 1e-3));
    });
    test('Finding f4 areas', (){
      var area=areaUnderCurveWithRectangleRule(f4, 0, 5, 1000);
      expect(area, closeToPercentage(18.413159148, 1e-3));
    });
  });

  group('Area test 3', () {
    test('Finding f4 areas', (){
      var area=integrateAdaptiveMidpoint(f4, 0,5,2);
      expect(area, closeToPercentage(18.413159148, 0.001));
    });
  });
}

Matcher closeToPercentage(num value, double percent) {
  final delta=value * percent;
  return closeTo(value, delta);
}

double f1(double x) {
  return x*x;
}

double f2(double x) {
  return 2*x;
}

double f3(double x) {
  return sin(x)*sin(x);
}

double f4(double x) {
  return 1+x+sin(2*x);
}