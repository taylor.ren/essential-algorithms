import 'package:essential_algorithms/essential_algorithms.dart';
import 'package:test/test.dart';

void main() {
  group('A GCD test', () {
    test('Various GCD scenarios', () {
      expect(gcd(60,48),equals(12));
      expect(gcd(3003,4851),equals(231));
      expect(gcd(60,0),equals(60));
    });
    test('Various eGCD scenarios', () {
      var res=egcd(210,154);
      expect(res[2],equals(gcd(210,154)));
      res=egcd(3003, 4851);
      expect(res[2],equals(gcd(4851, 3003)));
      
    });
  });

  
}
