import 'package:essential_algorithms/essential_algorithms.dart';
import 'package:test/test.dart';

void main() {
  var eps=1e-6;
  group('Newton Methond to find Zero test', () {
    test('Finding f1 zeros1', (){
      var root=newtonMethod(f1, df1, 2, eps);
      expect(root, closeTo(1, eps));
    });
    test('Finding f1 zeros2', (){
      var root=newtonMethod(f1, df1, -2, eps);
      expect(root, closeTo(-1, eps));
    });
    test('Finding f2 zeros1', (){
      var root=newtonMethod(f2, df2, 2.0, eps,10);
      expect(root, closeTo(1.381967, eps));
    });
    test('Finding f2 zeros2', (){
      var root=newtonMethod(f2, df2, 4.0, eps,10);
      expect(root, closeTo(3.618034, eps));
    });
    test('Finding f2 zeros3', (){
      var root=newtonMethod(f2, df2, -12.0, eps,10);
      expect(root, closeTo(0.0, eps));
    });
  });

}

double f1 (double x) {
  return x*x*x*x-1;
}

double df1(double x) {
  return 4*x*x*x;
}

double f2(double x) {
  return x*x*x/5.0-x*x+x;
}

double df2(double x) {
  return 3.0*x*x/5.0-2.0*x+1;
}