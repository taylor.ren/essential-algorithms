import 'package:essential_algorithms/essential_algorithms.dart';
import 'dart:math';


void main() {
  var a=egcd(210, 154);
  print(a);
  print('210*${a[0]}+154*${a[1]}=${a[2]}=${210*a[0]+154*a[1]}');

  print('${exponentiate(2,17)} == ${pow(2,17)}');
  print('${exponentiate(2,32)} == ${pow(2,32)}');
  print('${exponentiate(2,33)} == ${pow(2,33)}');  
  print('${exponentiate(2,62)} == ${pow(2,62)}');  
  print('${exponentiate(2,63)-1} == ${pow(2,63)-1}');  

  print(findFactors(18));
  print(findFactors(102));
  print(findFactors(2*2*7*37*102));
  print(findFactors(10387565125425));
  print(findFactors(59273));
  print('${92149613*37} = ${findFactors(92149613*37)}');

  var max=100;
  var  list1=findPrimes(max);
  print(list1);

  print(isPrime(59293,30));  
  print(areaUnderCurveWithRectangleRule(f1, 0, 1,2));

}

double f1(double x) {
  return x;
}
