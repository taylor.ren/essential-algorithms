import 'dart:math';

int gcd(int a, int b) {
  while (b!=0) {
    var remainder= a%b;
    a=b;
    b=remainder;
  }
  return a;
}

List<int> egcd(int a, int b) {
  var r0=a;
  var r1=b;
  var x0=1;
  var x1=0;
  var y0=0;
  var y1=1;
  var r2, q2;
  var x2, y2;

  do {
    r2=r0%r1;
    q2=r0~/r1;
    x2=x0-q2*x1;
    y2=y0-q2*y1;

    //print ('q/r/x/y: $q2, $r2, $x2, $y2');
    r0=r1;
    r1=r2;
    x0=x1;
    x1=x2;
    y0=y1;
    y1=y2;
  } while (r2!=0);

  return [x0,y0,r0].toList();
}

int exponentiate(int e, int p) {
  var res=1;
  var factor=e;
  while(p!=0) {
    if(p%2==1) {
      res*=factor;
    }
    factor*=factor;
    p=p~/2;
  }

  return res;
}

List<int> findFactors(int number) {
  var factors=List<int>();
  while (number % 2 ==0) {
      factors.add(2);
      number=number~/2;
  }
  var i=3;
  var max_factor=sqrt(number);
  while(i<=max_factor) {
    while(number % i ==0) {
      factors.add(i);
      number=number~/i;
      max_factor=sqrt(number);
    }
    i+=2;
  }

  if(number>1) {
    factors.add(number);
  }

  return factors;
}

List<int> findPrimes(int max) {
  var is_composite=List<bool>(max+1);
  var primes=List<int>();

  is_composite.fillRange(0, max, false);
  for(var i=4;i<=max;i+=2) {
    is_composite[i]=true;
  }

  var next_prime=3;
  var stop=sqrt(max);

  while(next_prime<=stop) {
    for(var i=next_prime*2;i<=max;i+=next_prime) {
      is_composite[i]=true;
    }
    next_prime+=2;
    while(next_prime<=max && is_composite[next_prime]) {
      next_prime+=2;
    }
  }

  // TODO: is there a beter way instead of iterating the list?  
  for(var i=2;i<=max;i++) {
    if(!is_composite[i]){
      primes.add(i);
    }
  }
  return primes;
}

bool isPrime(int a, [int times=10]) {
  var rng=Random();
  for (var i=1;i<=times;i++) {
    var n = rng.nextDouble()*a;
    var n1=n.round();
    var res=exponentiate(n1, a-1);
    if(res%a!=1) {
      return false;
    }
  }
  return true;
}

double areaUnderCurveWithRectangleRule(Function f, double min, double max, [int intervals=200]) {
  var dx=(max-min)/intervals;
  var area=0.0;
  var x=min;
  for(var i=1;i<=intervals;i++) {
    var sub=dx*f(x);
    area+=sub;
    x+=dx;
  }

  return area;
}

double areaUnderCurveWithTrapezoidRule(Function f, double min, double max, [int intervals=200]) {
  var dx=(max-min)/intervals;
  var area=0.0;
  var x=min;
  for(var i=1;i<=intervals;i++) {
    var sub=dx*(f(x)+f(x+dx))/2;
    area+=sub;
    x+=dx;
  }

  return area;
}

double integrateAdaptiveMidpoint(Function f, double min, double max, [int intervals=20, double error=0.01]) {
  var dx=(max-min)/intervals;
  var area=0.0;
  var x=min;

  for (var i=1;i<=intervals;i++) {
    area+=sliceArea(f, x, x+dx, error);
    x+=dx;
  }

  return area;
}

double sliceArea(Function f, double x, double dx, double error) {
  var y1=f(x);
  var y2=f(dx);
  var xm=(x+dx)/2;
  var ym=f(xm);

  var a12=(dx-x)*(y1+y2)/2.0;
  var a1m=(xm-x)*(y1+ym)/2.0;
  var am2=(dx-xm)*(ym+y2)/2.0;
  var a1m2=a1m+am2;

  var eps=(a1m2-a12)/a12;
  if(eps.abs()<error) {
    return a1m2;
  }

  return sliceArea(f, x, xm, error)+sliceArea(f, xm, dx, error);
}

double newtonMethod(Function f, Function df, double guess, double error, [int iteration=100]) {
  var x=guess;

  for(var i=1;i<=iteration;i++) {
    double y=f(x);
    if(y.abs()<error) {
      break;
    }
    x=x-y/df(x);
  }

  return x;
}